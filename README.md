### Biotools Importer is a utility to import biotools data from a git server into OpenEBench.

The utility is based on modified version of [JGit](https://github.com/redmitry/jgit) libarary based on [NIO2](https://jcp.org/en/jsr/detail?id=203) API and 
[Jimfs](https://github.com/google/jimfs) in-memory file system. It allows wirking with git repositories directly, without writing anything on a disk.

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/elixir/tools-platform/openebench-biotools-importer.git
>cd openebench-biotools-importer
>mvn install
```

It will create **biotools-git-importer.jar** file in the **openebench-biotools-importer/target/** directory.

Running:
```shell
>java -jar biotools-git-importer.jar -u user -p password
```

where:

* -h         - help
* --git      - git repository to get tools json files
* --branch   - remote branch to checkout (default: 'origin/master')
* --path     - remote path to search for files
* --user     - openebench user name
* --password - openebench user password

All parameters may be defined via **/META-INF/config.properties** and **/META-INF/openebench-repository.cfg**

> biotools.git = https://github.com/bio-tools/content.git  <BR/>
> biotools.branch = origin/master  <BR/>
> biotools.path = data  <BR>
> openebench.user = redmitry  <BR/>
> openebench.password = <BR/>

> openebench.uri.base=https://openebench.bsc.es/monitor