/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.biotools;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A tool to import the tools from bio.tools repository
 * 
 * @author Dmitry Repchevsky
 */

public class Main {
    
    public final static String HELP = 
            "biotools-git-importer [-u && -p]\n\n" +
            "parameters:\n\n" +
            "-h (--help)                - this help message\n" +
            "-g (--git)                 - git endpoint\n" +
            "-b (--branch)              - git remote branch ['origin/master']\n" +
            "--path                     - git path to the tools ['/']\n" +
            "-u (--user) 'username'     - OpenEBench username\n" +
            "-p (--password) 'password' - OpenEBench pasword\n\n" +
            "comment: in the absense of credentials the tool only simulates the activity.\n" +
            "example: >java -jar biotools-git-importer.jar\n";
    
    public static void main(String[] args) {
        Map<String, List<String>> params = parameters(args);
        
        if (params.get("-h") != null ||
            params.get("--help") != null) {
            System.out.println(HELP);
            System.exit(0);
        }
        
        List<String> g = params.get("-g");
        if (g == null) {
            g = params.get("--git");
        }
        final String git = g == null || g.isEmpty() ? Configuration.GIT : g.get(0);

        List<String> b = params.get("-b");
        if (b == null) {
            b = params.get("--branch");
        }
        final String branch = b == null || b.isEmpty() ? Configuration.BRANCH : b.get(0);

        final List<String> path = params.get("--path");

        List<String> u = params.get("-u");
        if (u == null) {
            u = params.get("--user");
        }

        List<String> p = params.get("-p");
        if (p == null) {
            p = params.get("--password");
        }

        final String user = u == null || u.isEmpty() ? Configuration.USER : u.get(0);
        final String password = p == null || p.isEmpty() ? Configuration.PASSWORD : p.get(0);
        
        final BiotoolsRepositoryImporter importer;
        if (user == null || user.isEmpty() || password == null || password.isEmpty()) {
            importer = new BiotoolsRepositoryImporter();
        } else {
            importer = new BiotoolsRepositoryImporter(user, password);
        }
        
        importer.load(git, branch, path == null || path.isEmpty() ? Configuration.PATH : path.get(0));
    }
    
    private static Map<String, List<String>> parameters(final String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-g":
                case "--git":
                case "-b":
                case "--branch":
                case "--path":
                case "-u":
                case "--user":
                case "-p":
                case "--password":
                case "-h":
                case "--help": values = parameters.get(arg);
                               if (values == null) {
                                   values = new ArrayList(); 
                                   parameters.put(arg, values);
                               }
                               break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }
}
