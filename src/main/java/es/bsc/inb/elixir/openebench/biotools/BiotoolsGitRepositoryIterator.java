/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.biotools;

import com.google.common.jimfs.Jimfs;
import java.io.Closeable;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;
import javax.json.JsonValue.ValueType;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.util.SystemReader;

/**
 * @author Dmitry Repchevsky
 */

public class BiotoolsGitRepositoryIterator 
        implements Iterator<JsonObject>, Closeable {

    private final DirectoryStream<Path> directories;
    private final Iterator<Path> iterator;
    
    public BiotoolsGitRepositoryIterator(
            final String git, 
            final String branch, 
            final String path) throws IOException {

        final int idx = branch.lastIndexOf('/');
        final String local = idx < 0 ? branch : branch.substring(idx + 1);
        
        SystemReader old = SystemReader.getInstance();
        SystemReader.setInstance(new JgitSytemReaderHack(old));

        com.google.common.jimfs.Configuration config = 
                com.google.common.jimfs.Configuration.unix().toBuilder()
                        .setWorkingDirectory("/")
                        .build();

        final FileSystem fs = Jimfs.newFileSystem(config);
        final Path root = fs.getPath("/");
        
        try (Git g = Git.cloneRepository()
                          .setURI(git)
                          .setDirectory(root)
                          .setBranch(local)
                          .setRemote(branch)
                          .call()) {
        } catch (GitAPIException ex) {
            Logger.getLogger(BiotoolsGitRepositoryIterator.class.getName()).log(Level.SEVERE, ex.getMessage());
            throw new IOException(ex);
        }

        directories = Files.newDirectoryStream(root.resolve(path));
        iterator = directories.iterator();
    }
    
    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public JsonObject next() {
        while (hasNext()) {
            final Path directory = iterator.next();
            final String dir = directory.getFileName().toString();
            final Path file_path = directory.resolve(dir + ".biotools.json");

            if (Files.exists(file_path)) {
                try (JsonReader reader = Json.createReader(Files.newBufferedReader(file_path))) {
                    final JsonStructure structure = reader.read();
                    if (structure.getValueType() == ValueType.OBJECT) {
                        return structure.asJsonObject();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(BiotoolsGitRepositoryIterator.class.getName()).log(Level.SEVERE, ex.getMessage());
                }
            } else {
                Logger.getLogger(BiotoolsGitRepositoryIterator.class.getName()).log(Level.INFO, "no biotools description found: {0}", dir);
            }
        }
        
        return null;
    }

    @Override
    public void close() throws IOException {
        directories.close();
    }
}
